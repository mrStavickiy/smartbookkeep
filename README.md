**После git clone:**

*1) Инсталируем вендоры*


```
#!php

php composer.phar install
```


*2) Setting up Permissions http://symfony.com/doc/current/book/installation.html*
- Устанавливаем права на папки, в случае если операционка linux

*3) Включаev ресурсы, расположенные в директории Resources/public нашего бандла
*

```
#!php

php app/console assets:install web --symlink
```


*4) Генерируем скрипты и стили*


```
#!php

php app/console assetic:dump
```


*5) Генерируем таблицы*


```
#!php

app/console doctrine:schema:update --force
```


*6) Загружаем данные в бд*

```
#!php


php app/console doctrine:fixtures:load --append
```


Изначальный пользователь в БД с ролью admin; 
Он не синхронизируется в SugarCRM, т.к. добавляется самый первый и служит как админ системы. Вывел только ему, для удобства ссылку на sandbox API и отображение списка пользователей. Все Организации синхронизируются в ЦРМ, при регистрации. Но для этого нужно выполнить п. 7

```
#!php

email: admin@example.com
password: 11111
```



7) *Настройка интеграции* 

а) Со стороны системы Бухгалтерия:  Smart/Bundle/BookkeepBundle/Resources/config/parameters.yml
В нём 3 параметра:

```
#!php

parameters:
    smart_bookkeep.crm.url: http://{sugarcrm|suitecrm.com}/service/v4_1/rest.php
    smart_bookkeep.crm.username: admin
    smart_bookkeep.crm.password: 1
```


б) Со стороны SugarCRM или SuiteCRM
    Устанавливаем модули. Модули прикреплены в загрузки к этому репозиторию. Модули встанут на SugarCRM      систему 6.* и SuiteCRM до самой максимальной.
    Модуля два: 
1. Приложение Sync, которое отлавливает расхождения данных сервера и ЦРМ
2. Модуль Виды деятельности, в его комплект входят некоторые настройки и добавление полей для модуля Accounts(Организации | Котрагенты)

После установки можно еще раз выполнить восстановление, потому что бывают траблы с правами на linux-e(если не настроены). На Windows - не знаю. 
Далее заходим в файл {sugardir}/custom/SyncService/SyncService.php
и установить:

```
#!php

    /**
     *  Url адресс системы, entryPoint API.
     */
    public $url = 'http://smartcloud.test/api/v1/users';
```

Последний штрих - это установка библиотеки pecl_http-1.7.6(Для работы класса RestClient.php). Обязательно версию 1.7.6, т.к. на более новых возникают странные несогласованности.
Установка для линукса:

```
#!php

pecl install pecl_http-1.7.6
```

--> Добавить строчку в php.ini : extension=http.so;