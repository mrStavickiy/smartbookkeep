<?php

namespace Smart\Bundle\BookkeepBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Serializable;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Smart\Bundle\BookkeepBundle\Entity\UserRepository")
 */
class User implements UserInterface, Serializable, EquatableInterface
{
    /**
     * @ORM\Column(type="guid", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;
    
        
    /**
     * @ORM\Column(name="password", type="string")
     *
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="enterprise", type="string", length=255)
     */
    private $enterprise;

    /**
     * @var string
     *
     * @ORM\Column(name="tin", type="string", length=10)
     */
    private $tin;
    
   
    /**
     * Id сущности в sugarcrm системе
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(
     *      min = "36",
     *      max = "36",
     *      exactMessage = "Id объекта должно быть длиной {{ limit }} символа"
     * )
     *
     * @ORM\Column(name="sugar_id", type="string", length=36, nullable=true)
     */
    private $sugarId;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="user_role",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     *
     * @var ArrayCollection $userRoles
     */
    protected $userRoles;
    
    
    /**
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="user")
     **/
    private $userActivities;
    
    
    /**
     * Геттер для ролей пользователя.
     *
     * @return ArrayCollection A Doctrine ArrayCollection
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }
    
    /**
     * Геттер для массива ролей.
     * 
     * @return array An array of Role objects
     */
    public function getRoles()
    {
        return $this->getUserRoles()->toArray();
    }
    
    /**
     * Геттер для КВЭДов пользователя.
     *
     * @return ArrayCollection A Doctrine ArrayCollection
     */
    public function getUserActivities()
    {
        return $this->userActivities;
    }
    
    
    /**
     * Геттер для массива КВЭДов.
     * 
     * @return array An array of Role objects
     */
    public function getActivities()
    {
        return $this->getUserActivities()->toArray();
    }
    
    
    
    public function __construct() 
    {
        $this->userRoles = new ArrayCollection();
        $this->userActivities = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Get name
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set enterprise
     *
     * @param string $enterprise
     * @return User
     */
    public function setEnterprise($enterprise)
    {
        $this->enterprise = $enterprise;

        return $this;
    }

    /**
     * Get enterprise
     *
     * @return string 
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }
    
    
    
    /**
     * Get tin
     *
     * @return string 
     */
    public function getTin()
    {
        return $this->tin;
    }
    
    

    /**
     * Set tin
     *
     * @param string $tin
     * @return User
     */
    public function setTin($tin)
    {
        $this->tin = $tin;

        return $this;
    }


    
   
   

    /**
     * Set password
     *
     * @return string
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return \DateTime 
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    
    public function getSalt() {} 
    public function eraseCredentials() {} 


    
        /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $roles field !
         */
        return \serialize(array(
            $this->id,
            $this->name,
            $this->email,
            $this->password,
            serialize($this->userRoles),
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->name,
            $this->email,
            $this->password,
            //unserialize($this->userRoles),
        ) = \unserialize($serialized);

       // $this->userRoles = unserialize($roles);
    }

    
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

//        if ($this->salt !== $user->getSalt()) {
//            return false;
//        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }


    /**
     * Add userRoles
     *
     * @param \Smart\Bundle\BookkeepBundle\Entity\Role $userRoles
     * @return User
     */
    public function addUserRole(\Smart\Bundle\BookkeepBundle\Entity\Role $userRoles)
    {
        $this->userRoles[] = $userRoles;

        return $this;
    }

    /**
     * Remove userRoles
     *
     * @param \Smart\Bundle\BookkeepBundle\Entity\Role $userRoles
     */
    public function removeUserRole(\Smart\Bundle\BookkeepBundle\Entity\Role $userRoles)
    {
        $this->userRoles->removeElement($userRoles);
    }

    /**
     * Add userActivities
     *
     * @param \Smart\Bundle\BookkeepBundle\Entity\Activity $userActivities
     * @return User
     */
    public function addUserActivity(\Smart\Bundle\BookkeepBundle\Entity\Activity $userActivities)
    {
        $this->userActivities[] = $userActivities;

        return $this;
    }

    /**
     * Remove userActivities
     *
     * @param \Smart\Bundle\BookkeepBundle\Entity\Activity $userActivities
     */
    public function removeUserActivity(\Smart\Bundle\BookkeepBundle\Entity\Activity $userActivities)
    {
        $this->userActivities->removeElement($userActivities);
    }

    /**
     * Set sugarId
     *
     * @param string $sugarId
     * @return User
     */
    public function setSugarId($sugarId)
    {
        $this->sugarId = $sugarId;

        return $this;
    }

    /**
     * Get sugarId
     *
     * @return string 
     */
    public function getSugarId()
    {
        return $this->sugarId;
    }
}
