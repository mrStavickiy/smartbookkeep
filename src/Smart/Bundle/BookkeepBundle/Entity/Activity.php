<?php

namespace Smart\Bundle\BookkeepBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Activity
 *
 * @ORM\Table(name="activities",
 *        indexes={@ORM\Index(name="code_idx", columns={"code"})}
 * )
 * @ORM\Entity
 */
class Activity
{
    /**
     * @ORM\Column(type="guid", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="main", type="boolean")
     */
    private $main;
    
    
    /**
     * Id сущности в sugarcrm системе
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(
     *      min = "36",
     *      max = "36",
     *      exactMessage = "Id сущности должно быть длиной {{ limit }} символа"
     * )
     *
     * @ORM\Column(name="sugar_id", type="string", length=36, nullable=true)
     */
    private $sugarId;
    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userActivities")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Activity
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Activity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set user
     *
     * @param \Smart\Bundle\BookkeepBundle\Entity\User $user
     * @return Activity
     */
    public function setUser(\Smart\Bundle\BookkeepBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Smart\Bundle\BookkeepBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set main
     *
     * @param boolean $main
     * @return Activity
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean 
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set sugarId
     *
     * @param string $sugarId
     * @return Activity
     */
    public function setSugarId($sugarId)
    {
        $this->sugarId = $sugarId;

        return $this;
    }

    /**
     * Get sugarId
     *
     * @return string 
     */
    public function getSugarId()
    {
        return $this->sugarId;
    }
}
