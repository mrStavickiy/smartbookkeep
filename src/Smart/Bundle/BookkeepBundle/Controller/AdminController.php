<?php

namespace Smart\Bundle\BookkeepBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

use Smart\Bundle\BookkeepBundle\Entity\User;
use Smart\Bundle\BookkeepBundle\Entity\Role;
use Smart\Bundle\BookkeepBundle\Controller\ApiController as Api;

class AdminController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SmartBookkeepBundle:Admin:index.html.twig', array('name' => $name));
    }
    
    public function loginAction() 
    {
        $request = $this->getRequest();
        $session = $request->getSession();
//        ini_set('xdebug.var_display_max_depth', 5);
//        ini_set('xdebug.var_display_max_children', 256);
//        ini_set('xdebug.var_display_max_data', 1024);
        $user = $this->getUser();
        // получить ошибки логина, если таковые имеются
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }
//        var_dump($user);
        if(!$user) {
            return $this->render('SmartBookkeepBundle:System:login.html.twig', array(
                'error'         => $error,
                'success'       => "",
            ));
        } else {
            return $this->redirect($this->generateUrl('smart_bookkeep_homepage'));
        }
    }
    
    public function registerAction() 
    {
        return $this->render('SmartBookkeepBundle:System:register.html.twig');
    }
    
    public function saveAction() 
    {
        $request = $this->getRequest();
        $manager = $this->getDoctrine()->getManager();
        
        //Role
        //Обычные пользователи добавляются только с ролью ROLE_USER
        $role = $this->getDoctrine()
        ->getRepository('SmartBookkeepBundle:Role')
        ->findOneBy( array( 'name' => 'ROLE_USER' ) );
        //////

        //User
        $user = new User();     
        $user->setName($request->get('_name'));
        $user->setPassword($request->get('_password'));
        $user->setEmail($request->get('_email'));
        $user->setTin($request->get('_tin'));
        $user->setEnterprise($request->get('_enterprise'));
        $user->getUserRoles()->add($role);
        $manager->persist($user);
        ////////
        
        //Выполняем SQL
        $manager->flush();
        
        ///Отправка данных в SugarCRM/SuiteCRM
        $api = new Api();
        $api->sendAccount($user, 'registersave', $this->container);
        /////
        return $this->render('SmartBookkeepBundle:System:login.html.twig', array(
            'error'         => array(),
            'success'       => array(
                'user_id' => $user->getId(),
                'message' => 'Регистрация прошла успешно!',
            ),
        ));
    }
}

