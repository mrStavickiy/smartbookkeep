<?php

namespace Smart\Bundle\BookkeepBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Stalk\RealtyBundle\Entity\User;
use Doctrine;
//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * PageApi controller.
 *
 */
class ApiController extends FOSRestController
{
    
    private $status;
    protected $container;

    public function __construct()
    {
        $this->status = 200;
    }

    /**
    * @Rest\View
    * 
    * @ApiDoc(
    *   resource = true,
    *   description="Get Account",
    *   statusCodes = {
    *       200 = "Returned when successful",
    *       404 = "Returned when the account is not found",
    *   }
    * )
    */
    public function getUserAction($id)
    {        
        
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('SmartBookkeepBundle:User')->find($id);
                 
        if ($entity) {
            //Пароль низя!
           $entity->setPassword(null); 
        } //else 
           //throw $this->createNotFoundException('Unable to find Property entity.'); 
        
        return $entity;
        
    } 
    
    
    /**
     * Обновление/Добвление данных об организациях
     * и видов их деятельности
     */
    public function sendAccount($user, $action, $container, $deleted = array()) {
        
        //$response = new Response();
        $this->container = $container;
        $em = $this->getDoctrine()->getManager();
        //Авторизируемся в SugarCRM
        $sessionId = $this->authSugarCRM();
        
        if ( $action == "registersave" ) {          
            $this->registerSave($sessionId, $user, $em);
        } elseif($action == "configsave") {
            $this->configSave($sessionId, $user, $em, $deleted);
        }
        
        $em->flush();    
        //Выход из SugarCRM
        $this->logoutSugarCRM($sessionId);        

        if (!empty($acc->id)) {
            return array('create' => true);
        } else {
            return array('error' => true);
        }
            
    }
    
    /**
     * Сохранение видов деятельности
     * @array deleted = array(
     *      ids..
     * ),
     *
     */
    private function configSave($sessionId, $user, $em, $deleted) 
    {
        $url = $this->container->getParameter('smart_bookkeep.crm.url');
        $acc = $this->getAccountSugarCrmBySiteId($sessionId, $user);
        if( !empty($acc->id) ) {
            $nameValueList = array();
            $activities = $user->getActivities();
            foreach ($activities as $act) {
                //Виды деятельности, у которых есть SugarCRM ID
                //Получится обновлять пакетом через set_entries
                //КВЭДЫ, у которых нет sugarId, к сожалению придётся
                //Добавлять поступательно, потому что API шуги
                //не возвращает ничего кроме SugarID в случае массового добавления
                $sugarId = $act->getSugarId();
                if( !empty($sugarId) ) {
                    $nameValueList[] = array(
                        array(
                            'name' => 'id',
                            'value' => $sugarId,
                        ),
                        array(
                            'name' => 'description',
                            'value' => $act->getDescription(),
                        ),
                        array(
                            'name' => 'main',
                            'value' => $act->getMain(),
                        ),
                        array(
                            'name' => 'deleted',
                            'value' => '0',
                        ),
                    );
                } else {
                    $new = array(
                            "session" => $sessionId,
                            "module_name" => 'KindActivities',
                            "name_value_list" => array(
                            array(
                                'name' => 'name',
                                'value' => $act->getCode(),
                            ),
                            array(
                                'name' => 'description',
                                'value' => $act->getDescription(),
                            ),
                            array(
                                'name' => 'site_id',
                                'value' => $act->getId(),
                            ),
                            array(
                                'name' => 'main',
                                'value' => $act->getMain(),
                            ),
                            array(
                                'name' => 'account_id',
                                'value' => $acc->id,
                            ),
                        ),
                    );
                    $object = $this->callSugarCRM("set_entry", $new, $url);
                    if( !empty($object) ) $act->setSugarId($object->id);
                    $em->persist($act); 
                }
            }
            
            //Удалённые
            foreach($deleted as $siddel) {
                $nameValueList[] = array(
                    array(
                        'name' => 'id',
                        'value' => $siddel,
                    ),
                    array(
                        'name' => 'deleted',
                        'value' => '1',
                    ),
                );
            }
            if ( !empty($nameValueList) ) {
                $parameters = array(
                    "session" => $sessionId,
                    "module_name" => 'KindActivities',
                    "name_value_list" => $nameValueList,
                );
                //Отправляем запрос, ответом будет список 
                $entries = $this->callSugarCRM("set_entries", $parameters, $url);
            }
            //log
            //self::logger('log/account.txt', $act);
        }
    }
    
    
    /**
     * Сохранение Предпринимателя при регистрации
     * 
     */
    private function registerSave($sessionId, $user, $em)
    {
        if(!empty($user->getName()) && !empty($user->getEmail())) {
            //Если организации c такии e-mail не существует - создаём его в SugarCRM
            if ( empty($this->getAccountSugarCrmByEmail($sessionId, $user)) ) {
                $acc = $this->createAccountCrm($sessionId, $user);
                if( $acc->id )
                    $user->setSugarId($acc->id);
                $em->persist($user);
            }  
            //log
           // self::logger('log/send-account.txt', $acc);  
        } 
    }
    
    
    /*
     * Авторизация SugarCRM
     * 
     * @return session_id
     */
    private function authSugarCRM()
    {
        
        $url = $this->container->getParameter('smart_bookkeep.crm.url');
        $username = $this->container->getParameter('smart_bookkeep.crm.username');
        $password = $this->container->getParameter('smart_bookkeep.crm.password');

        $loginParameters = array(
            "user_auth" => array(
                "user_name" => $username,
                "password" => md5($password),
                "version" => "1"
            ),
           "application_name" => "RestTest",
           "name_value_list" => array(),
        );

        $loginResult = $this->callSugarCRM("login", $loginParameters, $url);

	return $loginResult->id;
        
    }
    
    
    /*
     * Поиск организации в SugarCRM по Еmail
     * 
     * @return session_id
     */
    private function getAccountSugarCrmByEmail($sessionId, $user, $moduleName = "accounts")
    {
        
        $url = $this->container->getParameter('smart_bookkeep.crm.url');
        
        $email = $user->getEmail();
        
        $sql = "{$moduleName}.id in 
                    (SELECT eabr.bean_id 
                      FROM email_addr_bean_rel eabr 
                      JOIN email_addresses ea ON (ea.id = eabr.email_address_id) 
                      WHERE eabr.deleted=0 and ea.email_address LIKE '{$email}%')";

        $parameters = array(
             'session' => $sessionId,

             //The name of the module from which to retrieve records
             'module_name' => ucfirst($moduleName),

             //The SQL WHERE clause without the word "where".
             'query' => $sql,

             'order_by' => "",
             'offset' => 0,
             'select_fields' => array('id'),

             'max_results' => 1,
             'deleted' => 0,
             'favorites' => false,
        );

        $acc = $this->callSugarCRM("get_entry_list", $parameters, $url);

        if (empty($acc->result_count)) {
            return false;
        } else {
            return $acc->entry_list[0];
        }
        
    }
    
    
    /*
     * Поиск организации в SugarCRM
     * по ID Сайта
     * @return session_id
     */
    private function getAccountSugarCrmBySiteId($sessionId, $user)
    {
        
        $url = $this->container->getParameter('smart_bookkeep.crm.url');
        

       $sql = " accounts.site_id = '{$user->getId()}' ";

        $parameters = array(
             'session' => $sessionId,

             'module_name' => "Accounts",

             'query' => $sql,

             'order_by' => "",
             'offset' => 0,
             'select_fields' => array('id'),

             'max_results' => 1,
             'deleted' => 0,
             'favorites' => false,
        );

        $acc = $this->callSugarCRM("get_entry_list", $parameters, $url);

        if (empty($acc->result_count)) {
            return false;
        } else {
            return $acc->entry_list[0];
        }
        
    }
    
    
     /*
     * Создаём контрагента(организацию) в SugarCRM
     */
    private function createAccountCrm($sessionId, $user) {
        
        $url = $this->container->getParameter('smart_bookkeep.crm.url');

        $moduleName = "Accounts";
        
        $nameValueList = array(
            array(
                "name" => "name",
                "value" => $user->getEnterprise(),
            ),
            
            array(
                "name" => "site_id",
                "value" => $user->getId(),
            ),
            
            array(
                "name" => "email1",
                "value" => $user->getEmail(),
            ),

            array(
                "name" => "tin",
                "value" => $user->getTin(),
            ),

            array(
                "name" => "owner_name",
                "value" => $user->getName(),
            ),
        );
        //Формируем массив полей нового клиента
        $parameters = array(
            "session" => $sessionId,
            "module_name" => $moduleName,
            "name_value_list" => $nameValueList,
        );
        
        //Отправляем запрос, ответом будет объект с созданым контрагентом(организацией) и его полями
        $acc = $this->callSugarCRM("set_entry", $parameters, $url);
        
        //log
//        @self::logger('log/account.txt', $acc);
        
        return $acc;
        
    }
    
    
    /**
     * Logout SugarCRM
     * 
     * @return boolean
     */
    private function logoutSugarCRM($sessionId)
    {

        $url = $this->container->getParameter('smart_bookkeep.crm.url');
        
	return $this->callSugarCRM("logout", array("session" => $sessionId), $url);
        
    }
    
    
    
    
    /**
     * Универсальный вызов, любого метода REST SugarCRM
     */
    private function callSugarCRM($method, $parameters, $url)
    {

        ob_start();
        $curl_request = curl_init();

        curl_setopt($curl_request, CURLOPT_URL, $url);
        curl_setopt($curl_request, CURLOPT_POST, 1);
        curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl_request, CURLOPT_HEADER, 1);
        curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

        $jsonEncodedData = json_encode($parameters);

        $post = array(
             "method" => $method,
             "input_type" => "JSON",
             "response_type" => "JSON",
             "rest_data" => $jsonEncodedData
        );

        curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($curl_request);
        curl_close($curl_request);

        $result = explode("\r\n\r\n", $result, 2);
        $response = json_decode($result[1]);
        ob_end_flush();

        return $response;
        
    }
    
    /*
     * Функция логирования
     * 
     * @param string $filename
     * @param mixed $data
     * 
     */
    static function logger($filename, $data){

        $date = new \DateTime();
        
        $out = '=========================================' . PHP_EOL;        
        $out .= $date->format('Y-m-d H:i:s') . PHP_EOL;        
        $out .= '=========================================' . PHP_EOL;        
        $out .= var_export($data, TRUE) . PHP_EOL . PHP_EOL;
        
        return file_put_contents($filename, $out, FILE_APPEND);
        
    }
    
}