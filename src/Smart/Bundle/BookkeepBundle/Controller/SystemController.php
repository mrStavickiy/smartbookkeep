<?php

namespace Smart\Bundle\BookkeepBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Smart\Bundle\BookkeepBundle\Entity\Activity;
use Smart\Bundle\BookkeepBundle\Controller\ApiController as Api;
use Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Request;

class SystemController extends Controller
{
    public function indexAction()
    {
        if (!$this->get('security.context')->isGranted('ROLE_ADMIN')
            && !$this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        } 
        return $this->render('SmartBookkeepBundle:System:index.html.twig');
    }
    
    public function loginAction() 
    {
        return $this->render('SmartBookkeepBundle:System:register.html.twig');
    }
    
    public function listAction() 
    {
        $businessmens = $this->getDoctrine()
        ->getRepository('SmartBookkeepBundle:User')
        ->findAll();
        
        if ( !$businessmens ) {
            throw $this->createNotFoundException(
                'No users in table'
            );
        }
         
        return $this->render('SmartBookkeepBundle:System:businessmens.html.twig', array(
            'businessmens' => $businessmens,
        ));
    }
    
    public function getdictionaryAction(Request $req)      
    {
        $dictionary = $this->getDoctrine()->getRepository('SmartBookkeepBundle:Dictionary');
        $query = $dictionary->createQueryBuilder('a')
               ->where('a.code LIKE :term')
               ->setParameter('term', "%{$req->query->get('term')}%")
               ->getQuery()->getResult();
        $result = array();
        foreach ($query as $dict) {
            $result[] = array(
                'term' => $req->query->get('term'),
                'label' => $dict->getCode(),
                'desc' => $dict->getDescription(),
            ); 
        }
        $response = new Response(json_encode($result));
        return $response;
    }
    
    public function configAction() 
    {
        //Т.к. нет необходимости правки настроек предпринимателя 
        //другим пользователем в рамках данной задачи
        //Если будет необходимость в action передаём параметр {$id}
        //Поиск юзера будет по  $this->getDoctrine() .. ->find($id);
        $user = $this->getUser();
        ///////
        $dictionary = $this->getDoctrine()
        ->getRepository('SmartBookkeepBundle:Dictionary')
        ->findAll();        
        return $this->render('SmartBookkeepBundle:System:config.html.twig', 
                array( 
                    'user' => $user,
                    'activities' => $user->getActivities(),
                    'dictionary' => $dictionary,
                )
        );
    }
    
    public function saveconfAction() 
    {
        $request = $this->getRequest();
        $user = $this->getUser();
        $user_acts = $user->getActivities(); 
        $manager = $this->getDoctrine()->getManager();
        
        $activities = $request->request->get('activity')['acts'];
        $main_act_key = $request->request->get('act_main_group');
        $deleted = $this->removeEditAct($user_acts, $activities, $manager, $main_act_key);
        $this->addNewAct($activities, $user, $manager, $main_act_key);
        $manager->flush();
        ///Отправка данных в SugarCRM/SuiteCRM
        $api = new Api();
        $api->sendAccount($user, 'configsave', $this->container, $deleted);
        /////
        $this->get('session')->getFlashBag()->add('notice-successfully', 'Сохранено успешно!');
        return $this->redirect($this->generateUrl('smart_bookkeep_system_config'));
    }
    
    private function removeEditAct($user_acts, $activities, $manager, $main)
    {
        $deleted = array();
        foreach ($user_acts as $u_act) {
            $r = $this->value_exists($activities, 'act_id', $u_act->getId());
            if( $r === false) {
                $deleted[] = $u_act->getSugarId();
                $manager->remove($u_act);
            } else {
                $u_act->setDescription($activities[$r]['act_desc']);
                if ($r === $main) {
                    $u_act->setMain(true);
                } else {
                    $u_act->setMain(false);
                }
            }
        }
        return $deleted;
    }
    
    private function addNewAct($activities, $user, $manager, $main) 
    {
        if( !empty($activities) ) {
            foreach ($activities as $act) {
                if( empty($act['act_id']) ) {
                    $a = new Activity();
                    $a->setCode($act['act_code']);
                    $a->setDescription($act['act_desc']);
                    $a->setUser($user);
                    if($act['act_keyform'] === $main)
                        $a->setMain(true);
                    else 
                        $a->setMain(false);

                    $user->addUserActivity($a);
                    $manager->persist($user);
                    $manager->persist($a);
                }    
            }
        }
    }

    private function value_exists($array, $key, $value)
    {
        if (is_array($array)) {
            foreach ($array as $sub) {
                if (isset($sub[$key]) && $sub[$key] === $value)
                    return $sub['act_keyform'];
            }
        }
        return false;
    } 
}
